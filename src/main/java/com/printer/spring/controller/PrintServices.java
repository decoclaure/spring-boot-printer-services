package com.printer.spring.controller;

import java.awt.print.PrinterJob;
import java.util.HashMap;

import javax.print.PrintService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PrintServices {

	// get all printers
	@RequestMapping(value = "/printers", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllPrinters() {

		HashMap<String, Object> response = new HashMap<>();

		// Lookup for the available print services.
		PrintService[] printServices = PrinterJob.lookupPrintServices();

		if (printServices.length == 0) {
			response.put("mensaje", "no se encotraron servicios de impresoras");
			return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
		}
		int count = 1;
		// Iterates the print services and print out its name.
		for (PrintService printService : printServices) {
			String name = printService.getName();
			response.put("Impresora " + count, name);
			System.out.println("Name = " + name);
			count++;
		}

		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
}
